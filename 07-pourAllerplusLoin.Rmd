# Pour aller plus loin

### Plan de classement et règle de nommage {-}

- [Comment bien nommer ses fichiers numériques](https://doranum.fr/stockage-archivage/comment-nommer-fichiers/)
- [Nommage des fichiers numériques](https://www.pantheonsorbonne.fr/fileadmin/Service_archives/Gestion_des_archives_%C3%A9lectroniques/1_Nommage_V01.pdf)

### Reproductibilité {-}

- [Mettre en ligne son rapport d'analyse R markdown](https://tutorials.migale.inra.fr/posts/share-your-work/)
- [Traçabilité des
activités de recherche et gestion des connaissances](http://qualite-en-recherche.cnrs.fr/IMG/pdf/guide_tracabilite_activites_recherche_gestion_connaissances.pdf)
- [Mooc sur la reproductibilité des données de la recherche](https://www.fun-mooc.fr/courses/course-v1:inria+41016+self-paced/info)

