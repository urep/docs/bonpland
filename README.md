# BONPlAnD : BONnes Pratiques - Astuces dans la gestion de Données à l'UREP

Documentation HTML sur la gestion des données à l'UREP élaboré en `bookdwon`.

## Compilation du document

- Dans R :

```r
bookdown::render_book(input="index.rmd","all")
```

- Dans un terminal

```
make all
```

## GitLab Pages

Le fichier `.gitlab-ci.yml` permet de déployer le bookdown avec le système CI/CD (Intégration Continu/Déploiement Continu) de GitLab sur cette page : [https://urep.pages.mia.inra.fr/docs/bonpland](https://urep.pages.mia.inra.fr/docs/bonpland)
